'use strict';
const SEND_BUTTON_ID = '#sendMessageButton';
const MESSAGE_TEXT_AREA = '#messageInput';
const MESSAGE_LIST = '#messages';
const MESSAGE_LIST_WRAPPER = '#messagesWrapper';

var background = chrome.extension.getBackgroundPage();

$(document).ready(function () {
  $(SEND_BUTTON_ID).on('click', sendMessage);
  $(MESSAGE_TEXT_AREA).on('keyup', function (event) {
    if (event.keyCode === 13) {
      sendMessage()
    }
  })
  loadMessages(background.sessionMessages);
})

function loadMessages (messageObjects) {
  let html = messageObjects.reduce(function(z, f) {
    return z += formatMessageForChat(f);
  }, "");
  $(MESSAGE_LIST).html(html);
}

function addMessage (messageObject) {
  let html = formatMessageForChat(messageObject);
  $(MESSAGE_LIST).append(html);
  scrollToBottom();
}

function isSentByMe (messageObject) {
  return messageObject.ip === background.ipAddress
}

function formatMessageForChat (messageObject) {
  let listClass = isSentByMe(messageObject) ? 'local' : ''
  let messageString = `
  <li class="${listClass}"}>
    <div class="messageBody">
      <p>${messageObject.text}</p>
    </div>
    <div class="messageFooter">
      <label>${messageObject.timeSent}</label>
    </div>
  </li>`
  return messageString;
}

function sendMessage () {
  let messageText = $(MESSAGE_TEXT_AREA).val();
  if (messageText === '' || messageText === undefined) {
    return
  }
  clearMessageInputBox();
  background.sendMessage(messageText);
}

function clearMessageInputBox () {
  $(MESSAGE_TEXT_AREA).val('');
}

function scrollToBottom () {
  let scrollable = $(MESSAGE_LIST_WRAPPER)
  scrollable.animate({
    scrollTop: scrollable.prop('scrollHeight')
  }, 'slow');
}
