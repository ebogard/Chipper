'use strict';

const DEFAULT_MESSAGE_TYPE = 'message';

const IP_CONFIRMATION = 'ip_confirmation'

var ipAddress = null;

var sessionMessages = [];

var host = 'ws://node-express-env.fxjpqa8t2m.us-east-2.elasticbeanstalk.com:80';

var ws;


function openSocket () {
  ws = new WebSocket(host);
  console.log('Establishing websocket connection...');
  ws.onopen = function () {
    console.log('Established websocket connection with %s', host);
  }
  ws.onclose = function () {
    console.log('Websocket connection closed.')
    console.log('Attempting to reconnect...')
    openSocket();
  }
  ws.onmessage = function (event) {
    console.log('Received message');
    let messageObject = JSON.parse(event.data);
    if (messageObject.type == IP_CONFIRMATION) {
      ipAddress = messageObject.ip
    } else {
      sessionMessages.push(messageObject);
      chrome.extension.getViews({type: 'popup'}).forEach(function(view) {
        view.addMessage(messageObject);
      })
    }
  }
}

function sendMessage (messageText) {
  let encodedMessage = encodeMessageWithMetaData(messageText);
  ws.send(encodedMessage);
}

function encodeMessageWithMetaData (messageText) {
  let currentTime = new Date().toTimeString();
  //TODO: Add name
  let messageObject = {
    type: DEFAULT_MESSAGE_TYPE,
    ip: ipAddress,
    timeSent: currentTime,
    text: messageText
  }
  return JSON.stringify(messageObject);
}

(function () {
  openSocket();
})();
